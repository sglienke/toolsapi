unit DebugDCUHelper.Main;

interface

uses
  ToolsAPI;

implementation

uses
  Classes,
  StrUtils,
  SysUtils,
  ToolsAPI.ProjectManagerMenu;

type
  TProjectMenuItemCreatorNotifier = class(TNotifierObject, IOTAProjectMenuItemCreatorNotifier)
  private
    function IsSystemDebugEnabled(const project: IOTAProject): Boolean;
    procedure ToggleSystemDebug(const menuContextList: IInterfaceList);
  public
    procedure AddMenu(const Project: IOTAProject; const IdentList: TStrings;
      const ProjectManagerMenuList: IInterfaceList; IsMultiSelect: Boolean);
  end;

var
  notifier: Integer;

procedure Install;
begin
  notifier := (BorlandIDEServices as IOTAProjectManager).AddMenuItemCreatorNotifier(TProjectMenuItemCreatorNotifier.Create);
end;

procedure Uninstall;
begin
  // Known issues: when uninstalling the package in the IDE this might cause a one time exception
  (BorlandIDEServices as IOTAProjectManager).RemoveMenuItemCreatorNotifier(notifier);
end;

const
  SystemDebugDCUPath = '$(BDSLIB)\$(Platform)\debug\system';

procedure TProjectMenuItemCreatorNotifier.AddMenu(const Project: IOTAProject;
  const IdentList: TStrings; const ProjectManagerMenuList: IInterfaceList;
  IsMultiSelect: Boolean);
var
  menu: TProjectManagerMenu;
begin
  if not IsMultiSelect and Assigned(Project)
    and (IdentList.IndexOf(sProjectContainer) <> -1) then
  begin
    menu := TProjectManagerMenu.Create;
    menu.Enabled := True;
    if IsSystemDebugEnabled(Project) then
      menu.Caption := 'Disable System Debug DCUs'
    else
      menu.Caption := 'Enable System Debug DCUs';
    menu.OnExecute := ToggleSystemDebug;
    menu.Position := pmmpUserBuild;
    ProjectManagerMenuList.Add(menu);
  end;
end;

function TProjectMenuItemCreatorNotifier.IsSystemDebugEnabled(
  const project: IOTAProject): Boolean;
var
  searchPath: string;
begin
  searchPath := (Project.ProjectOptions as IOTAProjectOptionsConfigurations)
    .ActiveConfiguration.Value['DCC_UnitSearchPath'];
  Result := ContainsText(searchPath, SystemDebugDCUPath);
end;

procedure TProjectMenuItemCreatorNotifier.ToggleSystemDebug(
  const menuContextList: IInterfaceList);
var
  i: Integer;
  menuContext: IOTAProjectMenuContext;
  activeConfiguration: IOTABuildConfiguration;
begin
  for i := 0 to menuContextList.Count - 1 do
    if Supports(menuContextList[i], IOTAProjectMenuContext, menuContext) then
    begin
      activeConfiguration := (menuContext.Project.ProjectOptions
        as IOTAProjectOptionsConfigurations).ActiveConfiguration;
      if IsSystemDebugEnabled(menuContext.Project) then
        activeConfiguration.Value['DCC_UnitSearchPath'] :=
          ReplaceText(activeConfiguration.Value['DCC_UnitSearchPath'], SystemDebugDCUPath, '')
      else
        activeConfiguration.Value['DCC_UnitSearchPath'] :=
          activeConfiguration.Value['DCC_UnitSearchPath'] + ';' + SystemDebugDCUPath;
      menuContext.Project.MarkModified;
    end;
end;

initialization
  Install;

finalization
  Uninstall;

end.
