# IDE Plugins and ToolsAPI extensions #

A collection of small IDE plugins and ToolsAPI extensions to make the use of it easier.

## DebugDCUHelper ##

A small IDE plugin to easily turn on and off debugging into low level system units. It adds a new menu item to the context menu in the project manager to turn that on and off. It then adds/removes '$(BDSLIB)\$(Platform)\debug\system' to the search path of the current build configuration.

To use this you need to move the units you want to turn on and off (usually System.dcu, SysInit.dcu and Variants.dcu are good candidates) from $(BDSLIB)\$(Platform)\debug to $(BDSLIB)\$(Platform)\debug\system

##### Known issues #####

When uninstalling the plugin it might raise a one time exception when invoking the context menu in the project manager.

*Copyright (c) 2015 Stefan Glienke*