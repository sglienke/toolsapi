{******************************************************************************}
{                                                                              }
{           ToolsAPI Extensions                                                }
{                                                                              }
{           Copyright (c) 2015 Stefan Glienke - All rights reserved            }
{                                                                              }
{           http://www.dsharp.org                                              }
{                                                                              }
{******************************************************************************}

unit ToolsAPI.LocalMenu;

interface

uses
  ToolsAPI;

type
  TLocalMenu = class(TNotifierObject, IOTANotifier, IOTALocalMenu)
  private
    fCaption: string;
    fChecked: Boolean;
    fEnabled: Boolean;
    fHelpContext: Integer;
    fName: string;
    fParent: string;
    fPosition: Integer;
    fVerb: string;
    function GetCaption: string;
    function GetChecked: Boolean;
    function GetEnabled: Boolean;
    function GetHelpContext: Integer;
    function GetName: string;
    function GetParent: string;
    function GetPosition: Integer;
    function GetVerb: string;
    procedure SetCaption(const Value: string);
    procedure SetChecked(Value: Boolean);
    procedure SetEnabled(Value: Boolean);
    procedure SetHelpContext(Value: Integer);
    procedure SetName(const Value: string);
    procedure SetParent(const Value: string);
    procedure SetPosition(Value: Integer);
    procedure SetVerb(const Value: string);
  public
    property Caption: string read GetCaption write SetCaption;
    property Checked: Boolean read GetChecked write SetChecked;
    property Enabled: Boolean read GetEnabled write SetEnabled;
    property HelpContext: Integer read GetHelpContext write SetHelpContext;
    property Name: string read GetName write SetName;
    property Parent: string read GetParent write SetParent;
    property Position: Integer read GetPosition write SetPosition;
    property Verb: string read GetVerb write SetVerb;
  end;

implementation

{ TLocalMenu }

function TLocalMenu.GetCaption: string;
begin
  Result := fCaption;
end;

function TLocalMenu.GetChecked: Boolean;
begin
  Result := fChecked;
end;

function TLocalMenu.GetEnabled: Boolean;
begin
  Result := fEnabled;
end;

function TLocalMenu.GetHelpContext: Integer;
begin
  Result := fHelpContext;
end;

function TLocalMenu.GetName: string;
begin
  Result := fName;
end;

function TLocalMenu.GetParent: string;
begin
  Result := fParent;
end;

function TLocalMenu.GetPosition: Integer;
begin
  Result := fPosition;
end;

function TLocalMenu.GetVerb: string;
begin
  Result := fVerb;
end;

procedure TLocalMenu.SetCaption(const Value: string);
begin
  fCaption := Value;
end;

procedure TLocalMenu.SetChecked(Value: Boolean);
begin
  fChecked := Value;
end;

procedure TLocalMenu.SetEnabled(Value: Boolean);
begin
  fEnabled := Value;
end;

procedure TLocalMenu.SetHelpContext(Value: Integer);
begin
  fHelpContext := Value;
end;

procedure TLocalMenu.SetName(const Value: string);
begin
  fName := Value;
end;

procedure TLocalMenu.SetParent(const Value: string);
begin
  fParent := Value;
end;

procedure TLocalMenu.SetPosition(Value: Integer);
begin
  fPosition := Value;
end;

procedure TLocalMenu.SetVerb(const Value: string);
begin
  fVerb := Value;
end;

end.
