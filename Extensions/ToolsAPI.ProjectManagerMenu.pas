{******************************************************************************}
{                                                                              }
{           ToolsAPI Extensions                                                }
{                                                                              }
{           Copyright (c) 2015 Stefan Glienke - All rights reserved            }
{                                                                              }
{           http://www.dsharp.org                                              }
{                                                                              }
{******************************************************************************}

unit ToolsAPI.ProjectManagerMenu;

interface

uses
  Classes,
  ToolsAPI,
  ToolsAPI.LocalMenu;

type
  TProjectManagerMenuExecuteEvent = procedure (const MenuContextList: IInterfaceList) of object;

  TProjectManagerMenu = class(TLocalMenu, IOTAProjectManagerMenu)
  private
    fIsMultiSelectable: Boolean;
    fOnExecute: TProjectManagerMenuExecuteEvent;
    function GetIsMultiSelectable: Boolean;
    procedure SetIsMultiSelectable(Value: Boolean);
    procedure Execute(const MenuContextList: IInterfaceList);
    function PreExecute(const MenuContextList: IInterfaceList): Boolean;
    function PostExecute(const MenuContextList: IInterfaceList): Boolean;
  public
    property IsMultiSelectable: Boolean
      read GetIsMultiSelectable write SetIsMultiSelectable;
    property OnExecute: TProjectManagerMenuExecuteEvent
      read fOnExecute write fOnExecute;
  end;

implementation

{ TProjectManagerMenu }

procedure TProjectManagerMenu.Execute(const MenuContextList: IInterfaceList);
begin
  if Assigned(fOnExecute) then
    fOnExecute(MenuContextList);
end;

function TProjectManagerMenu.GetIsMultiSelectable: Boolean;
begin
  Result := fIsMultiSelectable;
end;

function TProjectManagerMenu.PostExecute(
  const MenuContextList: IInterfaceList): Boolean;
begin
  Result := True;
end;

function TProjectManagerMenu.PreExecute(
  const MenuContextList: IInterfaceList): Boolean;
begin
  Result := True;
end;

procedure TProjectManagerMenu.SetIsMultiSelectable(Value: Boolean);
begin
  fIsMultiSelectable := Value;
end;

end.
